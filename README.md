# Accountable interview

### Install everything

```zsh
$ npm i
```

### Start the project

```zsh
$ yarn start
```

### Start the tests

```zsh
$ yarn test
```

---
